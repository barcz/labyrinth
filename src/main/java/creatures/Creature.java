package creatures;

public interface Creature {

    int getStrength();

    int getLife();

    void setLife(int newLife);

    String getName();
}
