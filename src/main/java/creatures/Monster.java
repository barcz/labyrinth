package creatures;

public class Monster implements Creature {

    private int strength;
    private int life;

    public Monster(int strength, int life) {

        this.strength = strength;
        this.life = life;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getLife() {
        return life;
    }

    @Override
    public String getName() {
        return "Monster";
    }

    public void setLife(int life) {
        this.life = life;
    }
}
