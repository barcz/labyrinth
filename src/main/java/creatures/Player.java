package creatures;

import java.util.HashSet;
import java.util.Set;

public class Player implements Creature {

    private final String name;
    private int strength;
    private int life;
    private Set<String> inventory;
    private int numberOfHits;
    private int successfulHits;
    private int maxLife;

    public Player(String name, int initialStrength, int initialLife) {
        this.name = name;
        this.strength = initialStrength;
        this.life = initialLife;
        this.maxLife = initialLife;
        inventory = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public Set<String> getInventory() {
        return inventory;
    }

    public void addToInventory(String thing) {
        inventory.add(thing);
    }

    public void showInventory() {
        System.out.println("Your inventory:");
        inventory.forEach(System.out::println);
    }

    public int getNumberOfHits() {
        return numberOfHits;
    }

    public void setNumberOfHits(int numberOfHits) {
        this.numberOfHits = numberOfHits;
    }

    public int getSuccessfulHits() {
        return successfulHits;
    }

    public void setSuccessfulHits(int successfulHits) {
        this.successfulHits = successfulHits;
    }

    public void showStats() {
        System.out.println("Name: " + getName());
        System.out.println("Life: " + getLife());
        System.out.println("Streangth: " + getStrength());
        showInventory();

    }

    public int getMaxLife() {
        return maxLife;
    }

    public void setMaxLife(int maxLife) {
        this.maxLife = maxLife;
    }
}
