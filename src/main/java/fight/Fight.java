package fight;

import creatures.Creature;
import creatures.Player;

import java.util.Iterator;
import java.util.Random;

public class Fight {

    private Creature player;
    private Creature defender;
    private Random random;

    public Fight(Creature player, Creature defender) {
        this.player = player;
        this.defender = defender;
        random = new Random();
    }

    public Result fight() {

        for (FightIterator it = new FightIterator(player, defender); it.hasNext(); it.next()) ;

        if(player.getLife() > 0) {
            Player p = (Player) player;
            double hits = (p.getSuccessfulHits() * 10.0) / (p.getNumberOfHits() * 10.0);
            if(hits > 0.6) {
                p.setStrength(p.getStrength() + 5);
                p.setMaxLife(p.getMaxLife() + 5);
            }
        }

        Result r =  player.getLife() > 0 ? Result.WIN : Result.LOOSE;
        if( r == Result.WIN) {
            System.out.println("You " + r);
        }

        return r;
    }

    private void fight(Creature c1, Creature c2) {
        int luck = random.nextInt(10);
        if (luck > 5) {
            int damage = c1.getStrength() + (luck > 8 ? 5 : 0);
            c2.setLife(c2.getLife() - damage);

            if (c1 instanceof Player) {
                Player p = (Player) c1;
                p.setNumberOfHits(p.getNumberOfHits() + 1);
                p.setSuccessfulHits(p.getSuccessfulHits() + 1);
            }

            System.out.println(c1.getName() + " hit " + c2.getName() + ". Damage: " + damage);

        } else {
            if (c1 instanceof Player) {
                Player p = (Player) c1;
                p.setNumberOfHits(p.getNumberOfHits() + 1);
            }
            System.out.println(c1.getName() + " missed " + c2.getName());
        }

    }

    class FightIterator implements Iterator<Creature> {

        private Creature player;
        private Creature defender;
        private boolean playerNext;

        public FightIterator(Creature player, Creature defender) {
            this.player = player;
            this.defender = defender;
            this.playerNext = true;
            ((Player) player).setNumberOfHits(0);
            ((Player) player).setSuccessfulHits(0);
        }

        @Override
        public boolean hasNext() {
            return player.getLife() > 0 && defender.getLife() > 0;
        }

        @Override
        public Creature next() {
            if (playerNext) {
                playerNext = false;
                fight(player, defender);
                return player;
            } else {
                playerNext = true;
                fight(defender, player);
                return defender;
            }
        }
    }

}
