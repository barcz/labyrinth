package game;

import creatures.Player;
import room.Room;

public class Dig {
    private Room currentRoom;
    private Player player;

    public Dig(Room currentRoom, Player player) {
        this.currentRoom = currentRoom;
        this.player = player;
    }

    public Result dig() {
        if (currentRoom.getThing() != null) {
            if (currentRoom.getThing().equalsIgnoreCase("hillock")) {
                System.out.println("You found a silver key!");
                currentRoom.setThing(null);
                player.addToInventory("SILVER KEY");
            } else {
                System.out.println("You can't dig here anything");
            }
        } else {
            System.out.println("There is nothing in this room");
        }
        return new Result(currentRoom, player);
    }
}
