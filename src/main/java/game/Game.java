package game;

import creatures.Monster;
import creatures.Player;
import fight.Fight;
import fight.Result;
import room.ContainersAndThings;
import room.CreateRooms;
import room.Room;

import java.util.Optional;
import java.util.Random;

public class Game {

    private void initialMessage() {
        System.out.println("Hello, welcome to the game");
        System.out.println("You are in a room");

    }

    private Player createPlayer() {
        System.out.println("Enter player name:");
        String name = System.console().readLine();
        return new Player(name, 10, 100);
    }

    private boolean fight(Player player, int numberOfMonsters, Monster currentMonster) {
        Random r = new Random();
        int isThereAMonster = r.nextInt(10);
        if (isThereAMonster > 7) {
            System.out.println("When you entered the room a monster attacked you!");
            numberOfMonsters++;
            currentMonster.setLife(numberOfMonsters * 5 + currentMonster.getLife());
            currentMonster.setStrength(numberOfMonsters * 2 + currentMonster.getStrength());

            Fight f = new Fight(player, currentMonster);
            return f.fight() == Result.WIN;
        }
        return true;
    }

    public void start() {
        initialMessage();
        boolean quit = false, win = false, death = false;
        int numberOfMonsters = 0;
        Player player = createPlayer();
        ContainersAndThings containersAndThings = new ContainersAndThings();
        Monster currentMonster = new Monster(8, 80);
        CreateRooms rooms = new CreateRooms(10, containersAndThings.getThingsToPut());
        Room currentRoom = rooms.getStartingRoom();
        System.out.println("enter HELP to show available commands / QUIT to quit game");
        currentRoom.desc();
        String command = System.console().readLine();
        while (!quit && !win && !death) {

            String[] splittedCommand = command.split(" ");

            if (splittedCommand.length > 0) {
                String commandName = splittedCommand[0];
                switch (commandName.toUpperCase()) {
                    case "TAKE":
                        Take t = new Take(currentRoom, player);
                        game.Result takeResult = t.take();
                        currentRoom = takeResult.getCurrentRoom();
                        player = takeResult.getCurrentPlayer();
                        break;

                    case "DIG":
                        Dig d = new Dig(currentRoom, player);
                        game.Result digResult = d.dig();
                        currentRoom = digResult.getCurrentRoom();
                        player = digResult.getCurrentPlayer();
                        break;

                    case "OPEN":
                        if (currentRoom.getThing() != null) {
                            if (currentRoom.getThing().equalsIgnoreCase("silver chest") &&
                                    player.getInventory().contains("SILVER KEY")) {
                                System.out.println("You found a golden key in the silver chest!");
                                currentRoom.setThing(null);
                                player.addToInventory("GOLDEN KEY");
                            } else if (currentRoom.getThing().equalsIgnoreCase("silver chest")) {
                                System.out.println("You need to find the silver key to open the silver chest!");
                            } else if (currentRoom.getThing().equalsIgnoreCase("golden chest") &&
                                    player.getInventory().contains("GOLDEN KEY")) {
                                System.out.println("You found the exit key");
                                player.addToInventory("EXIT KEY");
                                currentRoom.setThing(null);
                            } else if (currentRoom.getThing().equalsIgnoreCase("golden chest")) {
                                System.out.println("You need to find the golden key to open the golden chest!");
                            } else if (currentRoom.getThing().equalsIgnoreCase("exit door") && player.getInventory().contains("EXIT KEY")) {
                                System.out.println("Congratulations ! You are out !");
                                win = true;
                            } else if (currentRoom.getThing().equalsIgnoreCase("exit door")) {
                                System.out.println("You need the exit key to exit!");
                            } else {
                                System.out.println("There is nothing to open here");
                            }
                        } else {
                            System.out.println("There is nothing in this room");
                        }
                    case "GO":
                        if (splittedCommand.length > 1) {
                            String direction = splittedCommand[1];
                            Optional<Room> nextRoom = currentRoom.go(direction);
                            if (nextRoom.isPresent()) {
                                System.out.println("going " + direction);
                                currentRoom = nextRoom.get();
                                currentRoom.desc();
                                death = !fight(player, numberOfMonsters, currentMonster);
                            } else {
                                System.out.println("invalid direction");
                            }
                        }

                        break;
                    case "DESC":
                        currentRoom.desc();
                        break;
                    case "STATS":
                        player.showStats();
                        break;
                    case "INVENTORY":
                        player.showInventory();
                        break;
                    case "HEAL":
                        player.setLife(player.getMaxLife());
                        numberOfMonsters++;
                        break;
                    case "HELP":
                        System.out.println("GO <direction> - go that way");
                        System.out.println("TAKE - take something you found");
                        System.out.println("OPEN - open something what can be opened");
                        System.out.println("DESC - describe current room");
                        System.out.println("STATS - show player stats");
                        System.out.println("HEAL - get back your max life, but the monsters will be stronger");
                        System.out.println("HELP - show commands");
                        break;
                    case "QUIT":
                        System.out.println("Bye!");
                        quit = true;
                        break;
                }
            }
            if (!win && !death && !quit) {
                command = System.console().readLine();
            }

        }

        if (death) {
            System.out.println("You were killed by a monster!");
        }

        if (win) {
            System.out.println("Congratulations !");
        }
    }


}