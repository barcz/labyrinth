package game;

import creatures.Player;
import room.Room;

public class Result {
    private Room currentRoom;
    private Player currentPlayer;

    public Result(Room currentRoom, Player currentPlayer) {
        this.currentRoom = currentRoom;
        this.currentPlayer = currentPlayer;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
}
