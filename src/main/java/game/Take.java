package game;

import creatures.Player;
import room.Room;

public class Take {
    private Room currentRoom;
    private Player player;

    public Take(Room currentRoom, Player player) {
        this.currentRoom = currentRoom;
        this.player = player;
    }

    public Result take() {
        if (currentRoom.getThing() != null) {
            if (currentRoom.getThing().equalsIgnoreCase("shovel")) {
                currentRoom.setThing(null);
                player.addToInventory("SHOVEL");
                System.out.println("Now you have a shovel");
            } else {
                System.out.println("You can't take anything here");
            }
        } else {
            System.out.println("There is nothing in this room");
        }
        return new Result(currentRoom, player);
    }
}
