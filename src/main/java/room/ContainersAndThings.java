package room;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ContainersAndThings {


    private Map<String, String> containers = new HashMap<>();
    private Map<String, String> opens = new HashMap<>();

    private Set<String> thingsToPut = new HashSet<>();

    public ContainersAndThings() {
        containers.put("hillock", "silver key");
        containers.put("silver chest", "golden key");
        containers.put("golden chest", "exit key");

        opens.put("silver key", "silver chest");
        opens.put("golden key", "golden chest");
        opens.put("exit key", "exit");

        thingsToPut.add("shovel");
        thingsToPut.add("silver chest");
        thingsToPut.add("golden chest");
        thingsToPut.add("hillock");

    }

    public Map<String, String> getContainers() {
        return containers;
    }

    public Map<String, String> getOpens() {
        return opens;
    }

    public Set<String> getThingsToPut() {
        return thingsToPut;
    }
}
