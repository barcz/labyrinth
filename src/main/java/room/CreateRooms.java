package room;

import java.util.*;

public class CreateRooms {

    private Random random;
    private final Room startingRoom;
    private Set<String> thingsToPlace;


    public CreateRooms(int numberOfRooms, Set<String> thingsToPlace) {
        random = new Random();
        this.thingsToPlace = thingsToPlace;
        startingRoom = new Room(RoomType.FORWARD);
        create(startingRoom, numberOfRooms);
    }
    public void create(Room currentRoom, int remaining) {
        if (remaining > 0) {

            List<RoomType> roomTypes = currentRoom.getRoomType().allowedTypeFromHere();
            RoomType next = roomTypes.get(random.nextInt(roomTypes.size()));
            Room nextRoom = currentRoom.addRoom(next);
            if(remaining == 4) {
                nextRoom.setThing("EXIT DOOR");
            }

            if( remaining % 2 == 1 && thingsToPlace.size() > 0) {
                String thing = (String) thingsToPlace.toArray()[0];
                nextRoom.setThing(thing);
                thingsToPlace.remove(thing);
            }

            nextRoom.setBackRoom(currentRoom);
            create(nextRoom, remaining - 1);
        }
    }

    public Room getStartingRoom() {
        return startingRoom;
    }
}
