package room;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Room {

    private RoomType roomType;
    private Map<RoomType, Room> nextRooms;
    private String thing;


    public Room(RoomType roomType) {
        this.roomType = roomType;
        nextRooms = new HashMap<>();
    }

    public Room addRoom(RoomType roomType) {
        Room r = new Room(roomType);
        nextRooms.put(roomType, r);
        return r;
    }

    public void setBackRoom(Room room) {
        nextRooms.put(this.roomType.getBack(), room);
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public Map<RoomType, Room> getNextRooms() {
        return nextRooms;
    }


    public void desc() {
        String directionsFromHere = nextRooms.keySet().stream().map(t -> t.name()).collect(Collectors.joining(", "));

        System.out.println("You can go from here: " + directionsFromHere);

        if(thing != null) {
            System.out.printf("There is a %s in this room\n\r", thing);
        }
    }

    public Optional<Room> go(String direction) {
        Set<String> collect = nextRooms.keySet().stream().map(t -> t.name().toUpperCase()).collect(Collectors.toSet());
        if(collect.contains(direction.toUpperCase())) {
            return Optional.of(nextRooms.get(RoomType.valueOf(direction.toUpperCase())));
        }
        return Optional.empty();
    }

    public String getThing() {
        return thing;
    }

    public void setThing(String thing) {
        this.thing = thing;
    }
}
