package room;

import java.util.ArrayList;
import java.util.List;

public enum RoomType {
    LEFT, FORWARD, RIGHT, BACKWARD;

    public RoomType getBack() {
        RoomType res = null;
        switch (this) {
            case RIGHT:
                res = LEFT;
                break;
            case LEFT:
                res = RIGHT;
                break;
            case FORWARD:
                res = BACKWARD;
                break;
        }
        return res;
    }

    public List<RoomType> allowedTypeFromHere() {
        List<RoomType> result = new ArrayList<>();

        switch (this) {
            case LEFT:
                result.add(LEFT);
                result.add(FORWARD);
                break;
            case RIGHT:
                result.add(RIGHT);
                result.add(FORWARD);
                break;
            case FORWARD:
                result.add(LEFT);
                result.add(RIGHT);
                result.add(FORWARD);
                break;

        }

        return result;

    }

}
