package fight;

import creatures.Monster;
import creatures.Player;
import org.junit.Test;

import static org.junit.Assert.*;

public class FightTest {

    @Test
    public void playerShouldWinAgainstWeakEnemy() {
        Player testPlayer = new Player("TEST_PLAYER", 10, 100);
        Monster testMonster = new Monster(3, 75);

        Fight testFight = new Fight(testPlayer, testMonster);
        Result fightResult = testFight.fight();

        assertEquals("Player should win", Result.WIN, fightResult);

    }

    @Test
    public void playerShouldLooseAgainstStrongEnemy() {
        Player testPlayer = new Player("TEST_PLAYER", 10, 100);
        Monster testMonster = new Monster(15, 100);

        Fight testFight = new Fight(testPlayer, testMonster);
        Result fightResult = testFight.fight();

        assertEquals("Player should loose", Result.LOOSE, fightResult);

    }

}