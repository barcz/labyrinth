package game;

import creatures.Player;
import org.junit.Test;
import room.Room;
import room.RoomType;

import static org.junit.Assert.*;

public class DigTest {

    @Test
    public void shouldGetSilverKey() {
        Room testRoom = new Room(RoomType.FORWARD);
        testRoom.setThing("hillock");
        Player testPlayer = new Player("TEST_USER", 10, 100);
        Dig d = new Dig(testRoom, testPlayer);
        Result r = d.dig();
        assertTrue("Player should have the Silver Key", testPlayer.getInventory().contains("SILVER KEY"));
    }

    @Test
    public void shouldNotGetAnything() {
        Room testRoom = new Room(RoomType.FORWARD);
        Player testPlayer = new Player("TEST_USER", 10, 100);
        Dig d = new Dig(testRoom, testPlayer);
        Result r = d.dig();
        assertTrue("Player should have the Silver Key", testPlayer.getInventory().isEmpty());
    }

}