package game;

import creatures.Player;
import org.junit.Test;
import room.Room;
import room.RoomType;

import static org.junit.Assert.*;

public class TakeTest {

    @Test
    public void shouldGetShovel() {
        Room testRoom = new Room(RoomType.FORWARD);
        testRoom.setThing("SHOVEL");
        Player testPlayer = new Player("TEST_PLAYER", 10 , 100);
        Take testTake = new Take(testRoom, testPlayer);
        Result r = testTake.take();
        assertTrue("Player should have a SHOVEL", r.getCurrentPlayer().getInventory().contains("SHOVEL"));
    }

    @Test
    public void shouldNotGetShovel() {
        Room testRoom = new Room(RoomType.FORWARD);
        Player testPlayer = new Player("TEST_PLAYER", 10 , 100);
        Take testTake = new Take(testRoom, testPlayer);
        Result r = testTake.take();
        assertTrue("Player should have a SHOVEL", r.getCurrentPlayer().getInventory().isEmpty());
    }

}